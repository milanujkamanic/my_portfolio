function factorial(x) {
    let result = 1;
    if (x > 1) {
        for (let i = 1; i <= x; i++) {
            result = result * i;
        }
        return result;
    }
    else {
        return "x мора бити позитивно";
    }
    };

document.querySelector(".plus").addEventListener("click", function(){
    let sabirak1 = document.querySelector(".poljeUnosa1").value 
    let sabirak2 = document.querySelector(".poljeUnosa2").value
    document.querySelector(".rezultat").value = Number(sabirak1) + Number(sabirak2);  
    });

    document.querySelector(".minus").addEventListener("click", function(){
        let umanjenik = document.querySelector(".poljeUnosa1").value 
        let umanjilac = document.querySelector(".poljeUnosa2").value
        document.querySelector(".rezultat").value = Number(umanjenik) - Number(umanjilac);   
        });
    document.querySelector(".puta").addEventListener("click", function(){
        let mnozilac1 = document.querySelector(".poljeUnosa1").value 
        let mnozilac2 = document.querySelector(".poljeUnosa2").value
        document.querySelector(".rezultat").value = Number(mnozilac1) * Number(mnozilac2); 
       });
    document.querySelector(".podeljeno").addEventListener("click", function(){
        let deljenik = document.querySelector(".poljeUnosa1").value 
        let delilac = document.querySelector(".poljeUnosa2").value
        document.querySelector(".rezultat").value = Number(deljenik) / Number(delilac);  
        });
    document.querySelector(".odsto").addEventListener("click", function(){
        let glavnica = document.querySelector(".poljeUnosa1").value 
        let procenat = document.querySelector(".poljeUnosa2").value
        document.querySelector(".rezultat").value = Number(glavnica) * Number(procenat)/100;  
        });
    
    document.querySelector(".koren").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value 
            document.querySelector(".rezultat").value = Math.sqrt(x);
           });
    document.querySelector(".koren3").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value 
            document.querySelector(".rezultat").value = x ** (1/3);  
            });  
    document.querySelector(".na").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value;
            let n = document.querySelector(".poljeUnosa2").value;
            document.querySelector(".rezultat").value = x ** n;  
            });  
    document.querySelector(".fak").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value;
            document.querySelector(".rezultat").value = factorial(x);
            });   
            
    document.querySelector(".ln").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value; 
            document.querySelector(".rezultat").value = Math.log(x);  
            });   
    document.querySelector(".log").addEventListener("click", function(){
            let x = document.querySelector(".poljeUnosa1").value; 
            document.querySelector(".rezultat").value = Math.log10(x);  
            });   
        document.querySelector(".obrisi").addEventListener("click", function(){
                document.querySelector(".rezultat").value = "";  
                document.querySelector(".poljeUnosa1").value = "";
                document.querySelector(".poljeUnosa2").value = ""; 
                });
    
    var selection = "rad"

    function change_label_selection(){
        var label = document.getElementById("info")
        label.innerHTML = "<b>"+selection.toUpperCase()+"</b>"
     } 
       
       function calc_deg(){
        var element = document.getElementById("deg")
        element.style = " color: black; background-color: rgb(108, 253, 111)"
        var element2 = document.getElementById("rad")
        element2.style = "border solid"
       
        selection = "deg"
        change_label_selection()
     }
     function calc_rad(){
        var element2 = document.getElementById("rad")
        element2.style = "color: black; background-color: rgb(108, 253, 111)"
        var element = document.getElementById("deg")
        element.style = "border: solid"
       
        selection = "rad"
        change_label_selection()
     }

    function sin(){
        var x = document.querySelector(".poljeUnosa1").value;
        if (selection == "rad"){
            document.querySelector(".rezultat").value = Math.sin(x);
        } else if (selection == "deg"){
            document.querySelector(".rezultat").value = Math.sin(x * (Math.PI / 180));
        }
      }
       
   function cos(){
      var x = document.querySelector(".poljeUnosa1").value;
      if (selection == "rad"){
        document.querySelector(".rezultat").value = Math.cos(x);
      } else if (selection == "deg"){
        document.querySelector(".rezultat").value = Math.cos(x * (Math.PI / 180));
      }
   }
       
   function tan(){
      var x = document.querySelector(".poljeUnosa1").value;
      if (selection == "rad"){
        document.querySelector(".rezultat").value = Math.tan(x);
      } else if (selection == "deg"){
        document.querySelector(".rezultat").value = Math.tan(x * (Math.PI / 180))
          }
        }
